document.write('\
    <footer class="footer">\
        <div class="container">\
            <div class="row mb-5">\
                <div class="col-md-6 col-lg-4">\
                    <h3 class="heading-section">Junte-se ao Time</h3>\
                    <p class="mb-4">Gostou de nossas atividades e tem interesse em nos ajudar? Envie uma mensagem com o assunto "Voluntário" e falando um pouco sobre você.</p>\
                    <p>Envie sua inscrição para nós!</p>\
                    <p>\<a href="voluntariar.html" class="btn btn-primary px-4 py-3">Juntar-se</a></p>\
                </div>\
                \
                <div class="col-md-6 col-lg-4">\
                    <h3 class="heading-section">Quer Adotar?</h3>\
                    <p class="mb-4">Se interessou e quer adotar um gato? Envie uma mensagem com o assunto "Adoção: (Nome do gato)" e um texto sobre um pouco de você.</p>\
                    <p>Entraremos em contato o quanto antes!</p>\
                    <p>\<a href="adotar.html" class="btn btn-primary px-4 py-3">Adotar</a></p>\
                </div>\
                \
                <div class="col-md-6 col-lg-4">\
                    <div class="block-23">\
                        <h3 class="heading-section">Contato</h3>\
                        <ul>\
                            <li><span class="icon icon-map-marker"></span><span class="text"> Rod. Washington Luís, km 235 - SP-310, São Carlos, São Paulo, 13565-905</span></li>\
                            <li><span class="icon icon-phone"></span><span class="text">Telefone: (16) 3351-8111</span></a></li>\
                            <li><span class="icon icon-clock-o"></span><span class="text">Segunda a Sexta: 8h - 18h</span></li>\
                        </ul>\
                    </div>\
                </div>\
            </div>\
            \
            <div class="row pt-2">\
                <div class="col-md-12 text-center">\
                    <p>\
                        <!-- Link back to Colorlib can\'t be removed. Template is licensed under CC BY 3.0. -->\
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos direitos reservados | Este template é feito com <i class="icon-heart" aria-hidden="true"></i> por <a href="https://colorlib.com" target="_blank" class="text-primary">Colorlib</a>\
                        <!-- Link back to Colorlib can\'t be removed. Template is licensed under CC BY 3.0. -->\
                    </p>\
                </div>\
            </div>\
        </div>\
    </footer>\
');